#include "droneSupervisor.h"
using namespace std;

DroneSupervisor::DroneSupervisor() :
    state_estimator( std::string(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR), ModuleNames::ODOMETRY_STATE_ESTIMATOR),
    trajectory_controller( std::string(MODULE_NAME_TRAJECTORY_CONTROLLER), ModuleNames::TRAJECTORY_CONTROLLER),
    arucoeye( std::string(MODULE_NAME_ARUCO_EYE), ModuleNames::ARUCO_EYE),
    localizer( std::string(MODULE_NAME_LOCALIZER), ModuleNames::LOCALIZER),
    obstacle_processor( std::string(MODULE_NAME_OBSTACLE_PROCESSOR), ModuleNames::OBSTACLE_PROCESSOR),
    trajectory_planner( std::string(MODULE_NAME_TRAJECTORY_PLANNER), ModuleNames::TRAJECTORY_PLANNER),
    yaw_planner( std::string(MODULE_NAME_YAW_PLANNER), ModuleNames::YAW_PLANNER),
    mission_planner( std::string(MODULE_NAME_MISSION_PLANNER), ModuleNames::MISSION_PLANNER)
{
    modules_name_list.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    modules_name_list.push_back(MODULE_NAME_TRAJECTORY_CONTROLLER);
    modules_name_list.push_back(MODULE_NAME_ARUCO_EYE);
    modules_name_list.push_back(MODULE_NAME_LOCALIZER);
    modules_name_list.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
    modules_name_list.push_back(MODULE_NAME_TRAJECTORY_PLANNER);
    modules_name_list.push_back(MODULE_NAME_YAW_PLANNER);
    modules_name_list.push_back(MODULE_NAME_MISSION_PLANNER);

    modules_list.push_back(&state_estimator);
    modules_list.push_back(&trajectory_controller);
    modules_list.push_back(&arucoeye);
    modules_list.push_back(&localizer);
    modules_list.push_back(&obstacle_processor);
    modules_list.push_back(&trajectory_planner);
    modules_list.push_back(&yaw_planner);
    modules_list.push_back(&mission_planner);


}

DroneSupervisor::~DroneSupervisor() 
{

}

bool DroneSupervisor::batteryServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response)
{
    response.success = true;
    response.message = "ok";
    return true;
}

bool DroneSupervisor::wifiServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response)
{
    response.success = true;
    response.message = "ok";
    return true;
}

bool DroneSupervisor::moduleIsStartedServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response)
{

    if(request.module_name == "all")
    {
        response.ack = true;
        std::vector<std::string>::iterator it_name = modules_name_list.begin();
        std::vector<DroneModuleInterface*>::iterator it_module = modules_list.begin();
        for (it_name = modules_name_list.begin(); it_name != modules_name_list.end(); ++it_name, ++it_module)
        {
            if(!(*it_module)->isStarted())
            {
                response.ack = false;
                break;
            }

        }
    }
    else
    {

        response.ack = false;
        std::vector<std::string>::iterator it_name =  modules_name_list.begin();
        std::vector<DroneModuleInterface*>::iterator it_module = modules_list.begin();


        for (it_name; it_name != modules_name_list.end(); ++it_name, ++it_module)
        {
            if(request.module_name == *it_name)
            {
                if((*it_module)->isStarted())
                    response.ack = true;
            }
        }
    }


    return true;
}

bool DroneSupervisor::moduleIsOnlineServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response)
{
    if(request.module_name == "all")
    {
        response.ack = true;
        std::vector<std::string>::iterator it_name = modules_name_list.begin();
        std::vector<DroneModuleInterface*>::iterator it_module = modules_list.begin();
        for (it_name; it_name != modules_name_list.end(); ++it_name, ++it_module)
        {
            if(!(*it_module)->isOnline())
            {
                response.ack = false;
                break;
            }

        }
    }


    else
    {
        response.ack = false;
        std::vector<std::string>::iterator it_name =  modules_name_list.begin();
        std::vector<DroneModuleInterface*>::iterator it_module = modules_list.begin();


        for (it_name; it_name != modules_name_list.end(); ++it_name, ++it_module)
        {
            if(request.module_name == *it_name)
            {
                if((*it_module)->isOnline())
                    response.ack = true;
            }
        }
    }

    return true;
}

void DroneSupervisor::open(ros::NodeHandle & nIn) 
{
    n = nIn;

    DroneModule::open(n);

    state_estimator.open(n);
    trajectory_controller.open(n);
    arucoeye.open(n);
    localizer.open(n);
    obstacle_processor.open(n);
    trajectory_planner.open(n);
    yaw_planner.open(n);
    mission_planner.open(n);

    batteryServerSrv = n.advertiseService("batteryIsOk",&DroneSupervisor::batteryServCall,this);
    wifiServerSrv = n.advertiseService("wifiIsOk",&DroneSupervisor::wifiServCall,this);
    moduleIsStartedServerSrv = n.advertiseService("moduleIsStarted",&DroneSupervisor::moduleIsStartedServCall,this);
    moduleIsOnlineServerSrv = n.advertiseService("moduleIsOnline",&DroneSupervisor::moduleIsOnlineServCall,this);

}


