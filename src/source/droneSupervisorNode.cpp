#include <iostream>
#include <string>
#include <vector>


#include "ros/ros.h"

#include "droneSupervisor.h"

using namespace std;



int main(int argc, char **argv)
{
    ros::init(argc, argv, "Supervisor_Server");
    ros::NodeHandle n;

    DroneSupervisor MyDroneSupervisor;
    MyDroneSupervisor.open(n);

    ROS_INFO("Supervisor Service Ready.");
    ros::spin();

    return 0;
}





