#ifndef DRONESUPERVISOR_H
#define DRONESUPERVISOR_H

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

#include "droneModuleROS.h"

// Topic and Rosservice names
#include "dronemoduleinterface.h"
#include "droneMsgsROS/droneNavData.h"
#include "droneMsgsROS/dronePose.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Trigger.h"
#include "communication_definition.h"
#include <geometry_msgs/Vector3Stamped.h>
#include <droneMsgsROS/droneAltitude.h>
#include <droneMsgsROS/vector2Stamped.h>
#include <droneMsgsROS/battery.h>
#include <droneMsgsROS/droneStatus.h>
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/askForModule.h"


class DroneSupervisor : public DroneModule
{
private:
    ros::NodeHandle n;
    std::vector<std::string> modules_name_list;
    std::vector<DroneModuleInterface*> modules_list;


    ros::ServiceServer batteryServerSrv;
    ros::ServiceServer wifiServerSrv;
    ros::ServiceServer moduleIsStartedServerSrv;
    ros::ServiceServer moduleIsOnlineServerSrv;

    DroneModuleInterface state_estimator;
    DroneModuleInterface trajectory_controller;
    DroneModuleInterface arucoeye;
    DroneModuleInterface localizer;
    DroneModuleInterface obstacle_processor;
    DroneModuleInterface trajectory_planner;
    DroneModuleInterface yaw_planner;
    DroneModuleInterface mission_planner;



public:
    DroneSupervisor();
    ~DroneSupervisor();
    void open(ros::NodeHandle & nIn);
    bool batteryServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response);
    bool wifiServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response);
    bool moduleIsStartedServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response);
    bool moduleIsOnlineServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response);


};

#endif // DRONESUPERVISOR_H
